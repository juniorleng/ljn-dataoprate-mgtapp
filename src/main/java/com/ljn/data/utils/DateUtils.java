package com.ljn.data.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.Assert;
import org.springframework.util.StringUtils;

/**
 * @description:
 * @projectName：pop-shop-mgtapp
 * @className：DateUtil.java
 * @version 1.0
 */
public class DateUtils
{
	private DateUtils()
	{
	}

	private static final Logger log = LoggerFactory.getLogger(DateUtils.class);

	public static final String DEFAULT_DATE_FORMAT = "yyyy-MM-dd HH:mm:ss";
	public static final String UNDERLINE_DATE_FORMAT = "yyyy-MM-dd";
	public static final String SLASH_DATE_TIME_FORMAT = "yyyy/MM/dd HH:mm:ss";

	/**
	 * @description: String转Date
	 * @param dateString
	 * @param format
	 */
	public static Date strToDate(String dateString, String format)
	{
		if (StringUtils.isEmpty(dateString))
		{
			return null;
		}
		try
		{
			SimpleDateFormat sdf = new SimpleDateFormat(StringUtils.isEmpty(format) ? DEFAULT_DATE_FORMAT : format);
			return sdf.parse(dateString);
		}
		catch (ParseException e)
		{
			log.info("{}", e.getMessage());
		}
		return null;
	}

	/**
	 * @description:把字符串类型的时间转成long类型的时间
	 * @param dateString
	 */
	public static Long timeToLong(String dateString)
	{
		if (StringUtils.isEmpty(dateString))
		{
			return null;
		}
		try
		{
			SimpleDateFormat sdf = new SimpleDateFormat(DEFAULT_DATE_FORMAT);
			return sdf.parse(dateString).getTime();
		}
		catch (ParseException e)
		{
			log.info("{}", e.getMessage());
		}
		return null;
	}

	/**
	 * @description: 把字符串类型的时间转成long类型的时间
	 * @param dateString
	 * @param format
	 */
	public static Long timeToLong(String dateString, String format)
	{
		if (StringUtils.isEmpty(dateString))
		{
			return null;
		}
		try
		{
			SimpleDateFormat sdf = new SimpleDateFormat(format);
			return sdf.parse(dateString).getTime();
		}
		catch (ParseException e)
		{
			log.info("{}", e.getMessage());
		}
		return null;
	}

	/**
	 * @description: 将数据库的时间戳字符串转成标准的时间格式
	 * @param dateString
	 */
	public static String formateLongTime(String dateString, String format)
	{
		if (StringUtils.isEmpty(dateString))
		{
			return null;
		}
		SimpleDateFormat sdf = new SimpleDateFormat(StringUtils.isEmpty(format) ? DEFAULT_DATE_FORMAT : format);
		return sdf.format(new Date(Long.parseLong(dateString)));
	}

	/**
	 * @description: 设置年份,月数和天数,变更月份后的时间戳
	 * @param year
	 *            设置年数
	 * @param month
	 *            设置月数
	 * @param addMonth
	 *            负数:减去的月数;正数:加上的月数
	 * @param day
	 *            设置天数
	 */
	public static Long getSetTimestamp(Integer year, Integer month, Integer addMonth, Integer day, boolean isStartTime)
	{
		Calendar calendar = Calendar.getInstance();
		if (year != null)
			calendar.set(Calendar.YEAR, year);
		if (month != null)
		{
			if (month > 12 || month < 0)
			{
				Assert.notNull(null, "时间参数异常");
			}
			calendar.set(Calendar.MONTH, month - 1);
		}
		if (day == null)
			day = calendar.get(Calendar.DAY_OF_MONTH);

		calendar.set(Calendar.MONTH, calendar.get(Calendar.MONTH) + addMonth);
		calendar.set(Calendar.DAY_OF_MONTH, day);
		if (isStartTime)
		{
			calendar.set(Calendar.HOUR_OF_DAY, 0);
			calendar.set(Calendar.MINUTE, 0);
			calendar.set(Calendar.SECOND, 0);
			calendar.set(Calendar.MILLISECOND, 0);
		}
		else
		{
			calendar.set(Calendar.HOUR_OF_DAY, 23);
			calendar.set(Calendar.MINUTE, 59);
			calendar.set(Calendar.SECOND, 59);
			calendar.set(Calendar.MILLISECOND, 999);
		}
		return calendar.getTimeInMillis();
	}

	/**
	 * @description: 获取指定时间戳所在的月份
	 * @param timestamp
	 */
	public static int getMonthByTimestamp(Long timestamp)
	{
		Calendar cal = Calendar.getInstance();
		cal.setTimeInMillis(timestamp);

		return cal.get(Calendar.MONTH) + 1;
	}

	/**
	 * @description: 获取指定时间戳所在月份的开始时间戳
	 */
	public static Long getMonthStartTimestampByTimestamp(Long timestamp)
	{
		Calendar c = Calendar.getInstance();
		c.setTimeInMillis(timestamp);

		c.set(Calendar.DAY_OF_MONTH, 1); // 第一天
		c.set(Calendar.HOUR_OF_DAY, 0);
		c.set(Calendar.MINUTE, 0);
		c.set(Calendar.SECOND, 0);
		c.set(Calendar.MILLISECOND, 0);

		return c.getTimeInMillis();

	}

	/**
	 * @description: 获取指定时间戳所在月份的结束时间戳
	 */
	public static Long getMonthEndTimestampByTimestamp(Long timestamp)
	{
		Calendar c = Calendar.getInstance();
		c.setTimeInMillis(timestamp);

		c.set(Calendar.DAY_OF_MONTH, c.getActualMaximum(Calendar.DAY_OF_MONTH));
		c.set(Calendar.HOUR_OF_DAY, 23);
		c.set(Calendar.MINUTE, 59);
		c.set(Calendar.SECOND, 59);
		c.set(Calendar.MILLISECOND, 999);

		return c.getTimeInMillis();

	}

	/**
	 * @description: 获取指定年份，指定月份的结束时间戳
	 * @param year
	 * @param month
	 */
	public static Long getMonthEndTimeStampByYearAndMonth(Integer year, Integer month)
	{

		if (month < 1 || month > 12)
			throw new IllegalArgumentException("月份错误");

		Calendar c = Calendar.getInstance();
		c.set(Calendar.YEAR, year);
		c.set(Calendar.MONTH, month - 1);
		c.set(Calendar.DAY_OF_MONTH, c.getActualMaximum(Calendar.DAY_OF_MONTH));
		// 将小时至0
		c.set(Calendar.HOUR_OF_DAY, 23);
		// 将分钟至0
		c.set(Calendar.MINUTE, 59);
		// 将秒至0
		c.set(Calendar.SECOND, 59);
		// 将毫秒至0
		c.set(Calendar.MILLISECOND, 999);

		return c.getTimeInMillis();

	}

	/**
	 * @description: 获取当前月的第一天时间和最后一天时间
	 * @param isFirst
	 */
	public static Long getMonthTime(Boolean isFirst)
	{
		Calendar calendar = Calendar.getInstance();

		if (isFirst)
		{
			calendar.set(Calendar.DAY_OF_MONTH, 1);
			calendar.set(Calendar.HOUR_OF_DAY, 0);
			calendar.set(Calendar.MINUTE, 0);
			calendar.set(Calendar.SECOND, 0);
			calendar.set(Calendar.MILLISECOND, 0);

			return calendar.getTimeInMillis();
		}
		else
		{
			calendar.set(Calendar.DAY_OF_MONTH, calendar.getActualMaximum(Calendar.DAY_OF_MONTH));
			calendar.set(Calendar.HOUR_OF_DAY, 23);
			calendar.set(Calendar.MINUTE, 59);
			calendar.set(Calendar.SECOND, 59);
			calendar.set(Calendar.MILLISECOND, 999);

			return calendar.getTimeInMillis();
		}

	}

	/**
	 * @description: 获取当前时间之前或之后多少天的时间
	 * @param days
	 */
	public static Long getTimeCompNew(Integer days)
	{
		Calendar calendar = Calendar.getInstance();
		calendar.add(Calendar.DATE, days);
		return calendar.getTimeInMillis();

	}

	/**
	 * @description: 获取两个时间戳之间相差多少天
	 * @param time1
	 * @param time2
	 */
	public static int getTimeDiff(Long time1, Long time2)
	{
		long timedef = time2 - time1;
		double dayDouble = Math.ceil(timedef / (1000.0 * 3600 * 24));
		return (new Double(dayDouble)).intValue();
	}

	public static Date getDate(String s) {
		return getDate(s, (String)null);
	}

	public static Date getDate(String s, String format) {
		Date date;
		try {
			if (StringUtils.isEmpty(format)) {
				format = "yyyy-MM-dd HH:mm:ss";
			}

			date = (new SimpleDateFormat(format)).parse(s);
		} catch (Exception var4) {
			date = new Date(0L);
		}

		return date;
	}
	public static String formatDate(long date, String format) {
		return formatDate(new Date(date), format);
	}

	public static String formatDate(Date date, String format) {
		if (StringUtils.isEmpty(format)) {
			format = "yyyy-MM-dd HH:mm:ss";
		}

		return (new SimpleDateFormat(format)).format(date);
	}

}

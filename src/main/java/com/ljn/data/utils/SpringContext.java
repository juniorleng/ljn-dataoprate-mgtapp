package com.ljn.data.utils;

import java.util.Locale;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.MessageSourceResolvable;
/**
 * @description spring上下文
 */
public class SpringContext implements ApplicationContextAware
{

	private static ApplicationContext applicationContext;

	public static synchronized ApplicationContext getApplicationContext()
	{
		return applicationContext;
	}

	@Override
	public synchronized void setApplicationContext(ApplicationContext applicationContext)
	{
		SpringContext.applicationContext = applicationContext;
	}

	public static Object getBean(String name)
	{
		return applicationContext.getBean(name);
	}

	public static <T> T getBean(Class<T> requiredType)
	{
		return applicationContext.getBean(requiredType);
	}

	public static <T> T getBean(String name, Class<T> requiredType)
	{
		return applicationContext.getBean(name, requiredType);
	}

	public static boolean containsBean(String name)
	{
		return applicationContext.containsBean(name);
	}

	public static boolean isSingleton(String name)
	{
		return applicationContext.isSingleton(name);
	}

	public static Class<?> getType(String name)
	{
		return applicationContext.getType(name);
	}

	public static String[] getAliases(String name)
	{
		return applicationContext.getAliases(name);
	}

	public static String getMessage(String code, Object[] args, String defaultMessage, Locale locale)
	{
		return applicationContext.getMessage(code, args, defaultMessage, locale);
	}

	public static String getMessage(String code, Object[] args, Locale locale)
	{
		return applicationContext.getMessage(code, args, locale);
	}

	public static String getMessage(MessageSourceResolvable resolvable, Locale locale)
	{
		return applicationContext.getMessage(resolvable, locale);
	}

}

package com.ljn.data.utils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class PaginationUtils {
    private PaginationUtils() {
        super();
    }

    /**
     * 
     * @description: 分页数据组装
     * @param currentPage
     * @param numPerPage
     * @param totalCount
     * @param recordList
     */
    public static Map<String, Object> pageRes(int currentPage, int numPerPage, int totalCount, List<?> recordList) {
        int pageCount;
        int beginPageIndex;
        int endPageIndex;

        pageCount = ((totalCount + numPerPage - 1) / numPerPage);

        if (pageCount <= 10) {
            beginPageIndex = 1;
            endPageIndex = pageCount;
        } else {
            beginPageIndex = (currentPage - 4);
            endPageIndex = (currentPage + 5);

            if (beginPageIndex < 1) {
                beginPageIndex = 1;
                endPageIndex = 10;
            }

            if (endPageIndex > pageCount) {
                endPageIndex = pageCount;
                beginPageIndex = (pageCount - 10 + 1);
            }
        }

        Map<String, Object> pageMap = new HashMap<>();

        if (recordList == null) {
            recordList = new ArrayList<>();
        }

        pageMap.put("pageNo", currentPage);
        pageMap.put("pageSize", numPerPage);
        pageMap.put("totalCount", totalCount);
        pageMap.put("recordList", recordList);
        pageMap.put("pageCount", pageCount);
        pageMap.put("beginPageIndex", beginPageIndex);
        pageMap.put("endPageIndex", endPageIndex);

        return pageMap;
    }

    
}

//package com.ljn.data.utils;
//
//import java.io.IOException;
//import java.io.InputStream;
//import java.util.List;
//import java.util.Map;
//
///**
// * @description:
// * @projectName：poiUtil
// * @className：FileReadUtil.java
// */
//public class FileReadUtil
//{
//
//	private FileReadUtil()
//	{
//	}
//
//	private static final String SUFFIX_EXCEL2003 = ".xls";
//	private static final String SUFFIX_EXCEL2007 = ".xlsx";
//	private static final String SUFFIX_WPS_EXCEL_T = ".ett";
//	private static final String SUFFIX_WPS_EXCEL = ".et";
//
//	public static <T> Map<String, List<T>> importFile(InputStream is, Map<String, String> titleMap, String filename, Class<?> obj) throws IOException
//	{
//	    Map<String, List<T>> map = null;
//
//		if (filename.endsWith(SUFFIX_EXCEL2003) || filename.endsWith(SUFFIX_WPS_EXCEL_T) || filename.endsWith(SUFFIX_WPS_EXCEL))
//		{
//			map = ExcelUtils.importFromExcel(is, titleMap, filename, obj);
//		}
//		else if (filename.endsWith(SUFFIX_EXCEL2007))
//		{
//		    map = ExcelUtils.importFromExcel(is, titleMap, filename, obj);
//		}
//
//		return map;
//	}
//
//}

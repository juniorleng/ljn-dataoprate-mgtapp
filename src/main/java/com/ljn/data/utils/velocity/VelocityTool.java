package com.ljn.data.utils.velocity;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.ljn.data.utils.DateUtils;
public class VelocityTool
{
	private static Logger log = LoggerFactory.getLogger(VelocityTool.class);

	/**
	 * @param time
	 *            日期对象
	 * @param style
	 *            日期格式
	 * @return 返回格式化日期
	 */
	public static String formatTime(long time, String style)
	{
		if (time == 0L)
		{
			return "";
		}

		return DateUtils.formatDate(time, style);
	}

	/**
	 * @description: 格式化时间
	 */
	public static String getFormatTimeOther(Long timeString, String format)
	{
		int lLen = 10;
		if (timeString != null)
		{
			if (timeString == 0)
			{
				return null;
			}
			int len = Long.toString(timeString).length();
			if (len == lLen)
			{
				return DateUtils.formatDate(timeString * 1000, format);
			}
			else
			{
				return DateUtils.formatDate(timeString, format);
			}
		}
		return null;
	}


	public static String getFirstCategoryCodeByCode(String thirdCode)
	{
		if (thirdCode != null && thirdCode.length() > 4)
		{
			return thirdCode.substring(0, 2);
		}
		return "";
	}

}

package com.ljn.data.utils.velocity;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DecimalFormat;
/**
 * @version 1.0
 * @description: 需要在页面上调用的格式化时间的工具类
 */
public class NumberTool
{

	private NumberTool()
	{

	}

	public static final String NUMBER_PATTERN_1 = "####.##";

	public static final String formatNumber(Object number, String pattern)
	{
		if (number == null)
		{
			number = 0.0;
		}
		else
		{
			number = Double.parseDouble(String.valueOf(number));
		}
		if (pattern == null || "".equals(pattern))
		{
			pattern = NUMBER_PATTERN_1;
		}

		return new DecimalFormat(pattern).format(number);
	}

	/**
	 * 实现浮点数的乘法运算功能
	 *
	 * @param v1
	 *            被乘数
	 * @param v2
	 *            乘数
	 * @return v1×v2的积
	 */
	public static double multi(Object v1, Object v2)
	{
		double v11 = (null == v1) ? 0.0 : Double.parseDouble(v1.toString().trim());
		double v22 = (null == v2) ? 0.0 : Double.parseDouble(v2.toString().trim());

		BigDecimal b1 = new BigDecimal(Double.toString(v11));
		BigDecimal b2 = new BigDecimal(Double.toString(v22));

		return b1.multiply(b2).doubleValue();
	}

	/**
	 * 实现浮点数的除法运算功能
	 *
	 * @param v1
	 *            被除数
	 * @param v2
	 *            除数
	 * @return v1/v2
	 */
	public static double div(double v1, double v2)
	{
		BigDecimal b1 = BigDecimal.valueOf(v1);
		BigDecimal b2 = BigDecimal.valueOf(v2);

		return b1.divide(b2, 10, BigDecimal.ROUND_HALF_UP).doubleValue();
	}

	/**
	 * 全舍小数
	 *
	 * @param finalDouble
	 * @param num
	 *            小数位数
	 * @return
	 */
	public static double mathDouble(double finalDouble, int num)
	{
		DecimalFormat formater = new DecimalFormat();
		formater.setMaximumFractionDigits(num);
		formater.setGroupingSize(0);
		formater.setRoundingMode(RoundingMode.FLOOR);

		return Double.parseDouble(formater.format(finalDouble));
	}

	/**
	 * @param arg1
	 *            加数1(Object类型)
	 * @param arg2
	 *            加数2(Object类型)
	 * @description:实现浮点数的加法运算功能
	 */
	public static double add(Object arg1, Object arg2)
	{
		double v1 = arg1 == null ? 0.0 : Double.parseDouble(arg1.toString().trim());
		double v2 = arg2 == null ? 0.0 : Double.parseDouble(arg2.toString().trim());
		BigDecimal b1 = new BigDecimal(Double.toString(v1));
		BigDecimal b2 = new BigDecimal(Double.toString(v2));

		return b1.add(b2).doubleValue();
	}

	/**
	 * @param v1
	 * @param v2
	 * @return
	 * @description: 实现整数的加法运算功能
	 */
	public static Integer add(Integer v1, Integer v2)
	{
		BigDecimal b1 = new BigDecimal(Integer.toString(v1));
		BigDecimal b2 = new BigDecimal(Integer.toString(v2));

		return b1.add(b2).intValue();
	}

	/**
	 * 实现浮点数的减法运算功能
	 *
	 * @param v1
	 *            被减数
	 * @param v2
	 *            减数
	 * @return v1-v2的差
	 */
	public static double sub(Object arg1, Object arg2)
	{
		double v1 = arg1 == null ? 0.0 : Double.parseDouble(arg1.toString().trim());
		double v2 = arg2 == null ? 0.0 : Double.parseDouble(arg2.toString().trim());
		BigDecimal b1 = new BigDecimal(Double.toString(v1));
		BigDecimal b2 = new BigDecimal(Double.toString(v2));

		return b1.subtract(b2).doubleValue();
	}

	/**
	 * @param arg1
	 * @param arg2
	 * @description: 四舍五入小数
	 * @author:POP研发部 饶志胜
	 */
	public static double roundDouble(double finalDouble, int num)
	{
		DecimalFormat formater = new DecimalFormat();
		formater.setMaximumFractionDigits(num);
		formater.setMinimumFractionDigits(num);
		formater.setGroupingSize(0);
		formater.setRoundingMode(RoundingMode.HALF_UP);

		return Double.parseDouble(formater.format(finalDouble));
	}

	/**
	 * @param Object
	 *            传入对象
	 * @param num
	 *            保留小数位
	 * @description: 四舍五入小数
	 */
	public static String roundObject(Object object, int num)
	{
		DecimalFormat formater = new DecimalFormat();
		formater.setMaximumFractionDigits(num);
		formater.setMinimumFractionDigits(num);
		formater.setGroupingSize(0);
		formater.setRoundingMode(RoundingMode.HALF_UP);

		return formater.format(object);
	}

}

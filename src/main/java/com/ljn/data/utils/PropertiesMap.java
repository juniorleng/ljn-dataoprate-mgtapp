package com.ljn.data.utils;

import java.util.HashMap;
import java.util.Map;

/**
 * @description: 资源文件
 */
public class PropertiesMap
{

	private static final String SYS_TYPE = "sysType";
	private static final String SYS_MODULE = "sysModule";
	private static final String KEYWORD = "keyword";
	private static final String QUESTION = "question";
	private static final String ANSWER = "answer";
	private static final String MSG_LINK = "msgLink";
	private static final String DISABLED = "disabled";
	private static final String CRESTOR_ID = "creatorId";
	private static final String CREATOR = "creator";

	private PropertiesMap()
	{
	}

	private static final Map<String, String> titleMap = new HashMap<>();

	static
	{
	
		titleMap.put("钢银业务类型", SYS_TYPE);
		titleMap.put("所属功能模块", SYS_MODULE);
		titleMap.put("关健字", KEYWORD);
		titleMap.put("问题", QUESTION);
		titleMap.put("答案", ANSWER);
		titleMap.put("答案链接", MSG_LINK );
		titleMap.put("创建人", CREATOR);
	}

	public static Map<String, String> getTitleMap()
	{
		return titleMap;
	}
}

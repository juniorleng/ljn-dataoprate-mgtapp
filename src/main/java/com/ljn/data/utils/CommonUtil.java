package com.ljn.data.utils;


import java.util.HashMap;
import java.util.Map;
import org.springframework.util.StringUtils;

public class CommonUtil {

    private CommonUtil() {
        super();
    }

    /**
     * @description: 对连在一起的开始和结束时间字符串数组格式化
     * @param times 包括开始和结束时间的字符串数组
     */
    public static Map<String, Long> formatTime(String[] times) {
        Map<String, Long> returnMap = new HashMap<>();
        long startTime = 0;
        long endTime = 0;
        if (times.length == 2) {
            startTime = DateUtils.getDate(times[0].trim().replaceAll("/", "-") + " 00:00:00").getTime();
            endTime = DateUtils.getDate(times[1].trim().replaceAll("/", "-") + " 23:59:59").getTime();
        } else if (times.length == 1) {
            startTime = DateUtils.getDate(times[0].trim().replaceAll("/", "-") + " 00:00:00").getTime();
        }

        returnMap.put("startTime", startTime);
        returnMap.put("endTime", endTime);

        return returnMap;
    }

    /**
     * @description: 判断字符串是否存在obj中
     * @param str
     * @param obj
     */
    public static boolean indexOf(String str, Object obj) {
        boolean result = false;

        if (StringUtils.isEmpty(str)) {
            return true;
        }

        if (obj == null) {
            return false;
        } else {
            String objStr = obj.toString();
            int idx = objStr.indexOf(str);

            if (idx >= 0) {
                result = true;
            }
        }

        return result;
    }

    /**
     * 格式化html
     *
     * @param sHtml
     * @return
     */
    public static String formatHtml(String sHtml) {
        if (sHtml != null && sHtml.length() > 0) {
            final String quot = "&quot;";
            sHtml = sHtml.replace("<script>", "");
            sHtml = sHtml.replace("&", "&amp;");
            sHtml = sHtml.replace("\"", quot);
            sHtml = sHtml.replace("“", quot);
            sHtml = sHtml.replace("”", quot);
            sHtml = sHtml.replace("<", "&lt;");
            sHtml = sHtml.replace(">", "&gt;");
            sHtml = sHtml.replace("'", "&#39;");
            sHtml = sHtml.replace("\r\n", "");
            return sHtml;
        }
        return "";
    }

}

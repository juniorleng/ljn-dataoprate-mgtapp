package com.ljn.data.exception;



/**
 * @description: 组合服务异常

 */
public class aisFacadeException extends AbstractException
{

	private static final long serialVersionUID = 3523790452272481487L;

	public aisFacadeException(Integer code, Object... params)
	{
		super(code, params);
	}
}

package com.ljn.data.exception;

public abstract class AbstractException extends RuntimeException {
    private static final long serialVersionUID = 1L;
    protected Integer code;
    protected Object[] params;

    public AbstractException(Integer code, Object... params) {
        this.code = code;
        this.params = params;
    }

    public Integer getMessageCode() {
        return this.code;
    }

    public Object[] getMessageParams() {
        return this.params;
    }
}
package com.ljn.data.exception;



/**
 * 异常代码
 */
public class AisFacadeExceptionCode
{
	private AisFacadeExceptionCode()
	{

	}

	/**
	 * 单文件过大
	 */
	public static final Integer RESOURCE_FILE_LONG = 35036001;

	/**
	 * 资源单文件格式错误
	 */
	public static final Integer RESOURCE_FILE_PATTERN_ERROR = 35036002;

	/**
	 * 资源单文件上传异常
	 */
	public static final Integer RESOURCE_FILE_UPLOAD_ERROR = 35036003;

	/**
	 * 资源单文件解析异常
	 */
	public static final Integer RESOURCE_FILE_ANALYSE_ERROR = 35036004;


}

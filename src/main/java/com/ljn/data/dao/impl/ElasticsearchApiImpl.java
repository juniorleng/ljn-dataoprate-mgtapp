package com.ljn.data.dao.impl;

import com.ljn.data.constant.ResourceConstant;
import com.ljn.data.dao.ElasticsearchApi;
import java.io.IOException;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import javax.annotation.Resource;
import com.ljn.data.utils.ESSerializableUtil;
import org.elasticsearch.action.admin.indices.delete.DeleteIndexRequest;
import org.elasticsearch.action.bulk.BulkRequest;
import org.elasticsearch.action.bulk.BulkResponse;
import org.elasticsearch.action.delete.DeleteRequest;
import org.elasticsearch.action.get.GetRequest;
import org.elasticsearch.action.get.GetResponse;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.action.index.IndexResponse;
import org.elasticsearch.action.search.*;
import org.elasticsearch.action.support.WriteRequest;
import org.elasticsearch.action.update.UpdateRequest;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.client.indices.CreateIndexRequest;
import org.elasticsearch.client.indices.GetIndexRequest;
import org.elasticsearch.common.settings.Settings;
import org.elasticsearch.common.unit.TimeValue;
import org.elasticsearch.common.xcontent.XContentType;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.MatchAllQueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.index.reindex.BulkByScrollResponse;
import org.elasticsearch.index.reindex.DeleteByQueryRequest;
import org.elasticsearch.index.reindex.UpdateByQueryRequest;
import org.elasticsearch.script.Script;
import org.elasticsearch.script.ScriptType;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

@Service("elasticsearchApi")
public class ElasticsearchApiImpl implements ElasticsearchApi {

    private static final Logger logger = LoggerFactory.getLogger("businessLog");


    private static final String EXCEPTION_MESSAGE = "请求出现异常,";

    @Resource
    private RestHighLevelClient client;



    @Override
    public boolean createIndex(String index, Integer numberOfShards, Integer numberOfReplicas,
                               String json) throws IOException {
        boolean flag = true;
        //创建索引
        try {
            deleteIndex(index);
            CreateIndexRequest request = new CreateIndexRequest(index);
            //设置分片数量
            request.settings(Settings.builder().put("index.number_of_shards", numberOfShards == null ? 5 : numberOfShards)
                    .put("index.number_of_replicas", numberOfReplicas == null ? 1 : numberOfReplicas));
            request.mapping(json, XContentType.JSON);
            client.indices().create(request, RequestOptions.DEFAULT);
        } catch (IOException e) {
            logger.error(ResourceConstant.DUBBO_BLOG + "createIndex" + EXCEPTION_MESSAGE + "param={},{},{},{},error={}", index, numberOfShards, numberOfReplicas, json,e);
            throw e;
        }
        return flag;
    }


    @Override
    public void singleInsertData(String index, String id, String json) {
        IndexRequest request = new IndexRequest(index);
        request.id(id).source(json, XContentType.JSON);
        exeIndexRequest(request);
    }


    @Override
    public void batchInsertData(String index, Map<String, String> map) {
        BulkRequest bulkRequest = new BulkRequest();
        // 将其他请求绑定到批量请求上，不止是indexRequest，其他查询，获取，删除操作均可以添加到批量请求，统一操作
        for (Map.Entry<String, String> entry : map.entrySet()) {
            String json = entry.getValue();
            IndexRequest request = new IndexRequest(index);
            request.id(entry.getKey()).source(json, XContentType.JSON);
            bulkRequest.add(request);
        }
        bulkIndex(bulkRequest);
    }


    @Override
    public void emptyIndexDocument(String index){
        BoolQueryBuilder boolBuilder = QueryBuilders.boolQuery();
        MatchAllQueryBuilder termQueryBuilder = QueryBuilders.matchAllQuery();
        boolBuilder.must(termQueryBuilder);
        exeDeleteByQueryAsync(index, new SearchSourceBuilder().query(boolBuilder).toString());
    }


    @Override
    public void delDocumentById(String index, String id) {
        DeleteRequest deleteRequest = new DeleteRequest(index, id);
        this.deleteIndexRequest(deleteRequest);
    }


    @Override
    public void delDocumentByIds(String index, List<String> ids) {
        BulkRequest bulkRequest = new BulkRequest();
        for (String id : ids) {
            bulkRequest.add(new DeleteRequest(index, id));
        }
        bulkIndex(bulkRequest);
    }


    @Override
    public String getDocumentById(String index, String id) throws IOException {
        try {
            GetRequest getRequest = new GetRequest(index,id);
            GetResponse getResponse = client.get(getRequest, RequestOptions.DEFAULT);
            return getResponse.getSourceAsString();
        }catch (Exception e){
            logger.error(ResourceConstant.DUBBO_BLOG + "ElasticsearchOperateApi.getDocumentById param={}，{},error={}",index, id, e);
            throw  e;
        }
    }

    @Override
    public String getDocumentByIds(String index, List<String> ids) throws IOException {
        SearchSourceBuilder sourceBuilder = new SearchSourceBuilder();
        sourceBuilder.query(QueryBuilders.termsQuery("_id",ids));
        if(ids.size() >10000){
            sourceBuilder.size(10000);
        }else{
            sourceBuilder.size(ids.size());
        }
        sourceBuilder.from();
        sourceBuilder.trackTotalHits(true);
        return exeSearch(index, sourceBuilder.toString());
    }

    @Override
    public String exeSearch(String index, String sourceBuilderJSON) throws IOException {
        SearchRequest searchRequest = new SearchRequest(index);
        searchRequest.source(ESSerializableUtil.stringToSearchSourceBuilder(sourceBuilderJSON));
        try {
            return client.search(searchRequest,RequestOptions.DEFAULT).toString();
        } catch (IOException e) {
            logger.error(ResourceConstant.DUBBO_BLOG + "ElasticsearchOperateApi.exeSearch index={},sourceBuilderJSON={},error={}", index, sourceBuilderJSON, e);
            throw e;
        }
    }


    @Override
    public long exeDeleteByQuery(String index, String sourceBuilderJSON) {
        if(!StringUtils.isEmpty(index) && !StringUtils.isEmpty(sourceBuilderJSON)){
            try {
                BulkByScrollResponse response = client.deleteByQuery(newDeleteByQueryRequest(index, sourceBuilderJSON), RequestOptions.DEFAULT);
                return response.getStatus().getUpdated();
            } catch (IOException e) {
                logger.error(ResourceConstant.DUBBO_BLOG + "ElasticsearchOperateApi.exeDeleteByQuery" + EXCEPTION_MESSAGE + " index={},sourceBuilderJSON={}, error={}",index, sourceBuilderJSON, e);
            }
        }
        return -1;
    }


    @Override
    public String exeSearchAndScroll(String index, String sourceBuilderJSON, int timeValueMinutes) throws IOException {
        SearchRequest searchRequest = new SearchRequest(index);
        searchRequest.source(ESSerializableUtil.stringToSearchSourceBuilder(sourceBuilderJSON));
        // 设置游标有效时间
        searchRequest.scroll(TimeValue.timeValueMinutes(timeValueMinutes));
        try {
            return client.search(searchRequest,RequestOptions.DEFAULT).toString();
        } catch (IOException e) {
            logger.error(ResourceConstant.DUBBO_BLOG + "ElasticsearchOperateApi.exeSearchAndScroll index={},sourceBuilderJSON={},error={}", index, sourceBuilderJSON, e);
            throw e;
        }
    }


    @Override
    public String getByScroll(String scrollId, int timeValueMinutes) throws IOException {
        SearchScrollRequest scrollRequest = new SearchScrollRequest(scrollId);
        // 设置游标有效时间
        scrollRequest.scroll(TimeValue.timeValueMinutes(timeValueMinutes));
        return client.scroll(scrollRequest, RequestOptions.DEFAULT).toString();
    }


    @Override
    public boolean clearScroll(String scrollId) throws IOException {
        ClearScrollRequest clearScrollRequest = new ClearScrollRequest();
        clearScrollRequest.addScrollId(scrollId);
        ClearScrollResponse clearScrollResponse = client.clearScroll(clearScrollRequest, RequestOptions.DEFAULT);
        return clearScrollResponse.isSucceeded();
    }


    private DeleteByQueryRequest newDeleteByQueryRequest(String index, String sourceBuilderJSON) throws IOException {
        //参数为索引名，可以不指定，可以一个，可以多个
        DeleteByQueryRequest request = new DeleteByQueryRequest(index);
        // 更新时版本冲突
        request.setConflicts("proceed");
        // 设置查询条件，第一个参数是字段名，第二个参数是字段的值
        SearchSourceBuilder searchSourceBuilder = ESSerializableUtil.stringToSearchSourceBuilder(sourceBuilderJSON);
        request.setQuery(searchSourceBuilder.query());
        // 更新最大文档数  在不知道数量的情况下，不要赋值
        // 批次大小
        request.setBatchSize(10000);
        // 并行
        request.setSlices(2);
        // 使用滚动参数来控制“搜索上下文”存活的时间
        request.setScroll(TimeValue.timeValueMinutes(10));
        // 超时
        request.setTimeout(TimeValue.timeValueMinutes(2));
        // 刷新索引
        request.setRefresh(true);
        return request;
    }


    public void exeDeleteByQueryAsync(String index, String sourceBuilderJSON) {
        if(!StringUtils.isEmpty(index) && !StringUtils.isEmpty(sourceBuilderJSON)){
            try {
                client.deleteByQueryAsync(newDeleteByQueryRequest(index, sourceBuilderJSON), RequestOptions.DEFAULT, null);
            } catch (IOException e) {
                logger.error(ResourceConstant.DUBBO_BLOG + "ElasticsearchOperateApi.exeDeleteByQueryAsync index={},sourceBuilderJSON={}, error={}", index, sourceBuilderJSON, e);
            }
        }
    }


    private IndexResponse exeIndexRequest(IndexRequest request) {
        try {
            return client.index(request, RequestOptions.DEFAULT);
        } catch (IOException e) {
            logger.error(ResourceConstant.DUBBO_BLOG + "exeIndexRequest" + EXCEPTION_MESSAGE + "request={},error={}", request.toString(), e);
        }
        return null;
    }


    private void bulkIndex(BulkRequest bulkRequest) {
        try {
            BulkResponse bulkResponse = client.bulk(bulkRequest,RequestOptions.DEFAULT);
            if (bulkResponse.hasFailures()) {
                logger.error("{}FAILED ===>> 索引失败！{}", ResourceConstant.DUBBO_BLOG , bulkResponse.buildFailureMessage());
            }
        } catch (IOException e) {
            logger.error(ResourceConstant.DUBBO_BLOG + "bulkIndex" + EXCEPTION_MESSAGE + "bulkRequest={},error={}", bulkRequest.toString(), e);
            throw new RuntimeException(e.getMessage());
        }
    }


    private void deleteIndexRequest(DeleteRequest deleteRequest) {
        try {
            this.client.delete(deleteRequest,RequestOptions.DEFAULT);
        } catch (IOException e) {
            logger.error(ResourceConstant.DUBBO_BLOG + "deleteIndexRequest" + EXCEPTION_MESSAGE + "deleteRequest={},error={}", deleteRequest, e);
        }
    }

    private boolean exists(String indexName) {
        GetIndexRequest request = new GetIndexRequest(indexName);
        request.local(false);
        request.humanReadable(true);
        request.includeDefaults(false);
        try {
            return client.indices().exists(request, RequestOptions.DEFAULT);
        } catch (IOException e) {
            logger.error(ResourceConstant.DUBBO_BLOG + "exists" + EXCEPTION_MESSAGE + "indexName={},error={}", indexName, e);
            return false;
        }
    }


    private void deleteIndex(String indexName) throws IOException {
        boolean exists = exists(indexName);
        if(!exists) {
            //不存在就结束
            return ;
        }
        //索引存在，就执行删除
        DeleteIndexRequest request = new DeleteIndexRequest(indexName);
        request.timeout(TimeValue.timeValueMinutes(2));
        request.timeout("2m");
        try {
            client.indices().delete(request, RequestOptions.DEFAULT);
        } catch (IOException e) {
            logger.error(ResourceConstant.DUBBO_BLOG + "deleteIndex" + EXCEPTION_MESSAGE + "indexName={},error={}", indexName, e);
            throw e;
        }
    }


    @Override
    public long updateIndexSingleFieldByQuery(String indexName, String field, String value, String sourceBuilderJSON) throws IOException {
        SearchSourceBuilder searchSourceBuilder = ESSerializableUtil.stringToSearchSourceBuilder(sourceBuilderJSON);

        BoolQueryBuilder boolBuilder = QueryBuilders.boolQuery();
        MatchAllQueryBuilder termQueryBuilder = QueryBuilders.matchAllQuery();
        boolBuilder.must(termQueryBuilder);

        UpdateByQueryRequest updateByQueryRequest = new UpdateByQueryRequest(indexName);
        updateByQueryRequest.setQuery(searchSourceBuilder.query());
        updateByQueryRequest.setMaxDocs(10000);
        updateByQueryRequest.setScroll(TimeValue.timeValueMinutes(10));
        updateByQueryRequest.setScript(new Script(ScriptType.INLINE,
                "painless",
                "ctx._source." + field + " = '"+ value +"'",
                Collections.emptyMap()));
        BulkByScrollResponse response = client.updateByQuery(updateByQueryRequest,RequestOptions.DEFAULT);
        return response.getUpdated();
    }

    @Override
    public void batchUpdate(String indexName, Map<String, Map<String, Object>> jsonMap) throws IOException {
        //校验参数
        if (CollectionUtils.isEmpty(jsonMap)) {
            return;
        }
        //封装批量更新请求对象
        BulkRequest bulkRequest = new BulkRequest();
        for (Map.Entry<String, Map<String, Object>> entry : jsonMap.entrySet()) {
            Map<String, Object> map = entry.getValue();
            //更新的内容不能为空
            if(CollectionUtils.isEmpty(map)) {
                continue;
            }
            UpdateRequest updateRequest = new UpdateRequest(indexName, entry.getKey());
            updateRequest.doc(map);
            bulkRequest.add(updateRequest);
        }
        if(CollectionUtils.isEmpty(bulkRequest.requests())) {
            return;
        }
        bulkRequest.setRefreshPolicy(WriteRequest.RefreshPolicy.WAIT_UNTIL);
        //send批量更新
        client.bulk(bulkRequest, RequestOptions.DEFAULT);
    }

}

package com.ljn.data.dao;


import java.io.IOException;
import java.util.List;
import java.util.Map;

/**
* 功能描述: ES操作API
* @Author: Lengjn
* @Date: 2021/8/13 11:17
 */
public interface ElasticsearchApi {
    /**
     * @description 创建索引  如果已存在，会直接覆盖
     * @param index 索引名称
     * @param numberOfShards 索引的分片数 不清楚设置时，传空，实现中会默认为5
     * @param numberOfReplicas 索引的副本数 不清楚设置时，传空，实现中会默认为1
     * @param json 索引结构内容
     * @return 执行结果
     * @Author: Lengjn
     * @Date: 2021/8/13 11:15
     */
    boolean createIndex(String index, Integer numberOfShards, Integer numberOfReplicas, String json)
            throws IOException;

    /**
     * @description 单条插入数据
     * @param index 索引名称
     * @param id 文档主键id
     * @param json 文档内容
     * @return IndexResponse执行结果
     * @Author: Lengjn
     * @Date: 2021/8/13 11:15
     */
    void singleInsertData(String index, String id, String json);

    /**
     * @description 批量插入数据 1000条3秒半  10000条15秒
     * @param index 索引名称
     * @param map key为文档主键id，value为文档内容
     * @Author: Lengjn
     * @Date: 2021/8/13 11:15
     */
    void batchInsertData(String index, Map<String,String> map);

    /**
     * @description 根据索引名称清空索引内容  数据量40W时，耗时90秒
     * @param index 索引名称
     * @author Lengjn
     * @Date: 2021/8/13 11:15
     */
    void emptyIndexDocument(String index);

    /**
     * @description 根据id删除es数据
     * @param index 索引名称
     * @param id 数据id 该处的id统一指的索引自身唯一主键  _id
     * @author Lengjn
     * @Date: 2021/8/13 11:15
     */
    void delDocumentById(String index, String id);

    /**
     * @description 根据文档ids删除es数据  （PS：方法内部考虑了条件数不能超过1024的问题）  30000条的情况下大概是8秒
     * @param index 索引名称
     * @param ids 文档ids集合
     * @author Lengjn
     * @Date: 2021/8/13 11:15
     */
    void delDocumentByIds(String index, List<String> ids);

    /**
     * @description 根据文档id查询es数据
     * @param index 索引名称
     * @param id 文档ids集合
     * @author Lengjn
     * @Date: 2021/8/13 11:15
     * @return 返回内容为SourceAsString
     */
    String getDocumentById(String index, String id) throws IOException;

    /**
     * @description 根据文档ids查询es数据  （PS：方法内部考虑了条件数不能超过1024的问题）  最多查询10000条
     * @param index 索引名称
     * @param ids 文档ids集合
     * @author Lengjn
     * @Date: 2021/8/13 11:15
     * @return SearchResponse的json字符串
     */
    String getDocumentByIds(String index, List<String> ids) throws IOException;

    /**
     * @description 根据索引名称和查询对象，执行查询并返回查询结果
     * @param index 索引名称
     * @param sourceBuilderJSON 查询条件对象  sourceBuilder的json字符串
     * @return 查询结果  SearchResponse的json字符串
     * @author Lengjn
     * @createTime 2020/3/19 16:23
     * @return SearchResponse的json字符串
     */
    String exeSearch(String index, String sourceBuilderJSON) throws IOException;

    /**
     * @description 根据查询条件执行删除 只使用sourceBuilderJSON中的queryBuilder
     * @param index 索引名称
     * @param sourceBuilderJSON 查询条件对象  sourceBuilder的json字符串
     * @author Lengjn
     * @createTime 2020/3/19 16:24
     */
    long exeDeleteByQuery(String index, String sourceBuilderJSON);

    /**
     * @description 根据索引名称和查询对象，执行查询并返回查询结果
     * @param index 索引名称
     * @param sourceBuilderJSON 查询条件对象  sourceBuilder的json字符串
     * @param timeValueMinutes 游标有效时间 单位为分钟
     * @return 查询结果  SearchResponse的json字符串
     * @author Lengjn
     * @createTime 2020/06/20 16:23
     * @return SearchResponse的json字符串
     */
    String exeSearchAndScroll(String index, String sourceBuilderJSON, int timeValueMinutes) throws IOException;

    /**
     * @description 根据游标id，执行查询并返回查询结果
     * @param scrollId 游标id
     * @param timeValueMinutes 游标有效时间 单位为分
     * @return 查询结果  SearchResponse的json字符串
     * @author Lengjn
     * @createTime 2020/06/20 16:23
     * @return SearchResponse的json字符串
     */
    String getByScroll(String scrollId, int timeValueMinutes) throws IOException;

    /**
         * 根据游标id清除数据
         * @param scrollId 游标id
         * @return 是否成功
         */
    boolean clearScroll(String scrollId) throws IOException;

    /**
     * @description 根据查询条件局部更新某个字段  目前只支持一次性更新1W条数据
     * @param indexName 索引名称
     * @param field 目标字段
     * @param value 目标value
     * @param sourceBuilderJSON 查询条件对象
     * @return 修改条数
     */
    long updateIndexSingleFieldByQuery(String indexName, String field, String value, String sourceBuilderJSON) throws IOException;

    /**
     * @description 批量更新
     * @param indexName 索引名称
     * @param jsonMap key:文档ID value:要修改的内容
     */
    void batchUpdate(String indexName, Map<String, Map<String, Object>> jsonMap) throws IOException;
    
}

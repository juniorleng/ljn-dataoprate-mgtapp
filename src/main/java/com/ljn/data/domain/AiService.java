/*
package com.ljn.data.domain;

import com.ljn.data.entity.AiResource;
import com.ljn.data.entity.SearchParam;

import java.util.List;

public interface AiService {

    */
/**
     * 批量插入
     * @param EsSearchDatas
     * @Author Lengjn
     *//*

    public void addResource(List<AiResource> esSearchDatas);

    */
/**
     * 执行修改
     * @param EsSearchDatas
     * @Author Lengjn
     *//*

    public void updateResource(String id,AiResource resource);

    */
/**
     *批量删除
     * @param ids 资源id列表
     * @Author Lengjn
     *//*

    public void  delResource(List<String> ids);

    */
/**
     *分页查询
     * @param param 查询条件 pageNo 第几页  pageSize 每页条数
     * @Author Lengjn
     *//*

    public List<AiResource> queryPageResource(SearchParam param,Integer pageNo,Long pageSize);


    */
/**
     * 根据关键词搜索
     * @param keyWord 关键词
     * @return
     * @Author Lengjn
     *//*

    public List<AiResource> getResourceByKeyWord(String keyWord);

    */
/**
     * 根据问题搜索
     * @param question 问题
     * @return
     * @Author Lengjn
     *//*

    public List<AiResource> getResourceByQuestion(String question);

    */
/**
     * 查询最大ID
     * @param bytes :上传文件 fileName：文件名
     * @return
     * @author Lengjn
     * @createTime 2019年11月28日上午9:02:08
     *//*

    public AiResource getResourceById(String id);

    */
/**
     * 查询最大ID
     * @param bytes :上传文件 fileName：文件名
     * @return
     * @author Lengjn
     * @createTime 2019年11月28日上午9:02:08
     *//*

    public Long getMaxId();


    */
/**
     * 解析上传文件至es
     * @param bytes :上传文件 fileName：文件名
     * @return
     * @author Lengjn
     * @createTime 2019年11月28日上午9:02:08
     *//*

    public List<AiResource> uploadResource(byte[] bytes, String fileName, String filePath, Long userId, String remark,
        Long creatorId, String creatorName);


}
*/

package com.ljn.data.enums;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @description: 处理结果对象错误码枚举
 */
public enum HandleItemResultEnum
{
	/**
	 * 成功
	 */
	SUCESS("成功", 200),
	/**
	 * 品种数据为空
	 */
	CATEGORY_EMPTY("品种数据为空", 201),
	/**
	 * 材质数据为空
	 */
	MATERIAL_EMPTY("材质数据为空", 202),
	/**
	 * 规格数据为空
	 */
	SPEC_EMPTY("规格数据为空", 203),
	/**
	 * 产地数据为空
	 */
	FACTORY_EMPTY("产地数据为空", 204),

	/**
	 * SKU属性校验失败
	 */
	SKU_INVALID("SKU属性校验失败", 205),

	/**
	 * SKU已存在
	 */
	SKU_EXIST("SKU已存在", 206),

	/**
	 * SKU标准化化失败，（品种非标或规格非标或材质非标或钢厂非标）
	 */
	SKU_STANDARD_FAIL("SKU标准化化失败，（品种非标或规格非标或材质非标或钢厂非标）", 207);

	/** 描述 */
	private String desc;
	/** 枚举值 */
	private int value;

	private HandleItemResultEnum(String desc, int value)
	{
		this.desc = desc;
		this.value = value;
	}

	public int getValue()
	{
		return value;
	}

	public String getDesc()
	{
		return desc;
	}

	public static HandleItemResultEnum getEnum(int value)
	{
		HandleItemResultEnum resultEnum = null;
		HandleItemResultEnum[] enumAry = HandleItemResultEnum.values();

		for (int i = 0; i < enumAry.length; i++)
		{
			if (enumAry[i].getValue() == value)
			{
				resultEnum = enumAry[i];
				break;
			}
		}

		return resultEnum;
	}

	public static Map<String, Map<String, Object>> toMap()
	{
		HandleItemResultEnum[] ary = HandleItemResultEnum.values();
		Map<String, Map<String, Object>> enumMap = new HashMap<>();

		for (int num = 0; num < ary.length; num++)
		{
			Map<String, Object> map = new HashMap<>();
			String key = String.valueOf(getEnum(ary[num].getValue()));
			map.put("desc", ary[num].getDesc());
			map.put("value", ary[num].getValue());

			enumMap.put(key, map);
		}

		return enumMap;
	}

	public static List<Map<String, Object>> toList()
	{
		HandleItemResultEnum[] ary = HandleItemResultEnum.values();
		List<Map<String, Object>> list = new ArrayList<>();

		for (int i = 0; i < ary.length; i++)
		{
			Map<String, Object> map = new HashMap<>();
			map.put("desc", ary[i].getDesc());
			map.put("value", ary[i].getValue());

			list.add(map);
		}

		return list;
	}
}

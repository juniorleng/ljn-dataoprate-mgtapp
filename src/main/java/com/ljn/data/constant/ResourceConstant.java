package com.ljn.data.constant;

/**
 * @description: 常量类
 */
public class ResourceConstant {


    public static final int FILE_MAX_SIZE = 5;            // 默认每页条数

    public static final Long DEFAULT_NUMPAGE = 10L;    // 默认每页条数
    public static final Long DEFAULT_PAGE = 1L;        // 默认第一页

    public static final String FAIL = "fail";            // 默认每页条数

    /**
     * dubbo日志业务类型
     */
    public static final String DUBBO_BLOG = "dubbo|";
}

package com.ljn.data.entity;

import java.io.Serializable;

public abstract class AbstractLoginContext implements Serializable {
    private static final long serialVersionUID = 1L;
    private long lastAccessTime;
    public static final String LOGIN_CONTEXT_NAME = "_LJN_WEBCONTEXT_LOGIN_";

    public AbstractLoginContext() {
    }

    public abstract long getLoginUID();

    public abstract String getLoginName();

    public abstract String getLoginUName();

    public abstract long getLoginTime();

    public abstract String getLoginIpAddr();

    public long getLastAccessTime() {
        return this.lastAccessTime;
    }

    public void setLastAccessTime(long lastAccessTime) {
        this.lastAccessTime = lastAccessTime;
    }
}

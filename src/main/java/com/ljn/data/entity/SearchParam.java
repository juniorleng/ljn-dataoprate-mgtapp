package com.ljn.data.entity;

public class SearchParam {
    /*
     *业务类型
     */
    Integer sysType;

    /*
     *所属功能模块
     */
    String sysModule;

    /*
     *关健字
     */
    String keyword;

    /*
     *问题
     */
    String question;

    /*
     *答案
     */
    String answer;

    /*
     *答案链接
     */
    String msgLink;

    /*
     *是否启用0-启用 1禁用
     */
    Integer disabled;

    /*
     *创建人ID
     */
    String creatorId;

    
    /*
     *创建时间
     */
    Long createTime;
    
    /*
     *开始时间
     */
    Long startTime;
    
    /*
     *结束时间
     */
    Long endTime;

    public Integer getSysType() {
        return sysType;
    }

    public void setSysType(Integer sysType) {
        this.sysType = sysType;
    }

    public String getSysModule() {
        return sysModule;
    }

    public void setSysModule(String sysModule) {
        this.sysModule = sysModule;
    }

    public String getKeyword() {
        return keyword;
    }

    public void setKeyword(String keyword) {
        this.keyword = keyword;
    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }

    public String getMsgLink() {
        return msgLink;
    }

    public void setMsgLink(String msgLink) {
        this.msgLink = msgLink;
    }

    public String getCreatorId() {
        return creatorId;
    }

    public void setCreatorId(String creatorId) {
        this.creatorId = creatorId;
    }

   
    public Long getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Long createTime) {
        this.createTime = createTime;
    }

    public Long getStartTime() {
        return startTime;
    }

    public void setStartTime(Long startTime) {
        this.startTime = startTime;
    }

    public Long getEndTime() {
        return endTime;
    }

    public void setEndTime(Long endTime) {
        this.endTime = endTime;
    }

    public Integer getDisabled() {
        return disabled;
    }

    public void setDisabled(Integer disabled) {
        this.disabled = disabled;
    }
}

package com.ljn.data.entity;


import java.io.Serializable;
import java.util.Map;
import org.springframework.ui.ModelMap;

public final class PageView implements Serializable {
    private static final long serialVersionUID = 1L;
    private ModelMap model;

    public PageView() {
    }

    public PageView(Map<String, Object> model) {
        if (model != null) {
            this.getModelMap().addAllAttributes(model);
        }

    }

    public PageView addObject(String attributeName, Object attributeValue) {
        this.getModelMap().addAttribute(attributeName, attributeValue);
        return this;
    }

    public PageView addAllObjects(Map<String, Object> model) {
        this.getModelMap().addAllAttributes(model);
        return this;
    }

/*    public PageView formatDateObject(String attributeName) {
        return this.formatDateObject(attributeName, (String)null);
    }*/

/*    public PageView formatDateObject(String attributeName, String format) {
        if (this.getModelMap().containsKey(attributeName)) {
            Long attributeValue = (Long)this.getModelMap().get(attributeName);
            this.getModelMap().addAttribute(attributeName, DateUtils.formatDate(attributeValue, format));
        }

        return this;
    }*/

    public ModelMap getModelMap() {
        if (this.model == null) {
            this.model = new ModelMap();
        }

        return this.model;
    }

    public Object get(String attributeName) {
        return this.getModelMap().get(attributeName);
    }

    public String toString() {
        return this.getModelMap().toString();
    }

/*    public String toJsonString() {
        return this.getModelMap().toJsonString();
    }*/

    public static PageView createPageView() {
        return new PageView();
    }

    public static PageView createPageView(Map<String, Object> model) {
        return new PageView(model);
    }
}
package com.ljn.data.controller;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.ljn.data.dao.ElasticsearchApi;
import com.ljn.data.utils.ESSerializableUtil;
import lombok.extern.slf4j.Slf4j;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.SearchHits;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import java.util.*;

/**
 * @author Lengjn
 * @Description controller
 * @ClassName ResourceOrderController.java
 * @createTime 2021年08月13日 11:28:00
 */
@Slf4j(topic = "businessLog")
@RestController
@RequestMapping("/resource-order")
public class ResourceOrderController {

    @Autowired
    private ElasticsearchApi elasticsearchApi;

    private final String indexName = "resource_order";

    @GetMapping("/hello")
    public String getHello(){
        return "hello;";
    }

    @PostMapping("/add/{str}")
    public void addOrder(@PathVariable("str") String str){
        Map<String,Object> map = new HashMap<>();
        map.put("parentCategoryName",str);
        elasticsearchApi.singleInsertData(indexName, UUID.randomUUID().toString(), JSONObject.toJSONString(map));
    }

    @GetMapping("/list")
    public String getList(){
        List<String> result = new ArrayList<>();
        SearchSourceBuilder sourceBuilder = new SearchSourceBuilder();
        sourceBuilder.from(0);
        sourceBuilder.size(5000);
        BoolQueryBuilder boolQueryBuilder = new BoolQueryBuilder();
        boolQueryBuilder.must(QueryBuilders.matchAllQuery());
        sourceBuilder.query(boolQueryBuilder);
        try {
            SearchResponse searchResponse = ESSerializableUtil.stringToSearchResponse(elasticsearchApi.exeSearch(indexName, sourceBuilder.toString()));
            SearchHits hits = searchResponse.getHits();
            // 封装列表数据
            SearchHit[] searchHits = hits.getHits();
            if (searchHits != null && searchHits.length > 0) {
                for (SearchHit searchHit : searchHits) {
                    String source = searchHit.getSourceAsString();
                    if (!StringUtils.isEmpty(source)) {
                        result.add(source);
                    }
                }
            }
        } catch (Exception e) {
            log.error("", e);
        }
        return JSONArray.toJSONString(result);
    }

}

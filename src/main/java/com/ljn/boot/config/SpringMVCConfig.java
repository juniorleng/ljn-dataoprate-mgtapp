package com.ljn.boot.config;



import java.util.ArrayList;
import java.util.List;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.servlet.mvc.method.annotation.RequestMappingHandlerAdapter;


/**
 * @version 1.0.0
 * @description: SpringMVC配置
 */
@Configuration
//@AutoConfigureOrder(Ordered.HIGHEST_PRECEDENCE)
@ComponentScan(basePackages =
        {"com.ljn"})
//@EnableConfigurationProperties(VelocityProperties.class)
public class SpringMVCConfig {

    /**
     * 错误页面跳转
     */
    private static final String MESSAGE_EXCEPTION_VIEW = "/message/exception";

    /**
     * UTF-8编码
     */
    private static final String CHAR_SET_UTF_8 = "UTF-8";




    /**
     * @return
     * @description: springmvc请求映射处理器
     */
    @Bean
    public RequestMappingHandlerAdapter mappingHandlerAdapter() {
        RequestMappingHandlerAdapter adapter = new RequestMappingHandlerAdapter();
        List<HttpMessageConverter<?>> converters = new ArrayList<>(1);
        converters.add(new MappingJackson2HttpMessageConverter());
        adapter.setMessageConverters(converters);
        return adapter;
    }

}
